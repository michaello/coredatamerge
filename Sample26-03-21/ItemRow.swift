//
//  Row.swift
//  Sample26-03-21
//
//  Created by Mike Pyrka on 27/3/21.
//

import SwiftUI

struct ItemRow: View {
    
    @Environment(\.managedObjectContext) private var viewContext
    @ObservedObject var item: Item
    
    private var allCommentsIds: String {
        (item.mutableOrderedSetValue(forKey: "comments").array as! [Comment]).map(\.id!).joined(separator: ",")
    }
    private var allVotesIds: String {
        let comments: [Comment] = (item.mutableOrderedSetValue(forKey: "comments").array as! [Comment])
        let votes: [Vote] = comments.flatMap { $0.mutableOrderedSetValue(forKey: "votes").array as! [Vote] }
        return votes.map(\.id!).joined(separator: ",")
    }
    
    var body: some View {
        VStack {
            Text("Item at \(item.timestamp ?? Date(), formatter: itemFormatter)")
            Text("Id: \(item.id ?? "-")")
            Text("Comments: \(allCommentsIds)")
            Text("Votes: \(allVotesIds)")
            Text("isFault: \(item.isFault ? "yes" : "no")")
        }
    }
}
