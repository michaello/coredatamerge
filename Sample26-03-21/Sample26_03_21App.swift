//
//  Sample26_03_21App.swift
//  Sample26-03-21
//
//  Created by Mike Pyrka on 26/3/21.
//

import SwiftUI
import CoreData

@main
struct Sample26_03_21App: App {
    
    let persistenceController = PersistenceController.shared

    init() {
        persistenceController.container.viewContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
    }
    
    var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
