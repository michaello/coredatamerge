//
//  ItemDetailsView.swift
//  Sample26-03-21
//
//  Created by Mike Pyrka on 27/3/21.
//

import SwiftUI
import CoreData

struct ItemDetailsView: View {
    
    @Environment(\.managedObjectContext) private var viewContext
    @ObservedObject var item: Item
    @State var policy = ""
    @State var idx = 0
    
    var body: some View {
        VStack(spacing: 16.0) {
            Text("Merge Policy: \(policy)")
            Button(action: addItem) {
                Label("Add Item", systemImage: "plus")
            }
            Button(action: {
                (viewContext.fetchAll(entity: Item.self) +
                viewContext.fetchAll(entity: Comment.self) +
                    viewContext.fetchAll(entity: Vote.self)).forEach(viewContext.delete)
                try! viewContext.save()
            }) {
                Text("Delete All")
            }
            Button(action: {
                let all: [NSMergePolicy] = [
                    .error,
                    .rollback,
                    .overwrite,
                    .mergeByPropertyObjectTrump,
                    .mergeByPropertyStoreTrump,
                ]
                idx += 1
                viewContext.mergePolicy = all[idx % all.count]
                self.policy = mergePolicy
            }) {
                Text("Switch Merge policy")
            }
            VStack {
                Text("Items count: \(viewContext.fetchAll(entity: Item.self).count)")
                Text("Comments count: \(viewContext.fetchAll(entity: Comment.self).count)")
                Text("Votes count: \(viewContext.fetchAll(entity: Vote.self).count)")
            }
            ItemRow(item: item)
        }
        .onAppear {
            policy = mergePolicy
        }
    }

    private func addItem() {
        withAnimation {
            Item.generateNew(context: viewContext)

            do {
                try viewContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
    
    private var mergePolicy: String {
        switch viewContext.mergePolicy as! NSMergePolicy {
        case .error:
            return "error"
        case .rollback:
            return "rollback"
        case .overwrite:
            return "overwrite"
        case .mergeByPropertyObjectTrump:
            return "mergeByPropertyObjectTrump"
        case .mergeByPropertyStoreTrump:
            return "mergeByPropertyStoreTrump"
        default:
            fatalError()
        }
    }
}

extension Item {
    static func generateNew(context: NSManagedObjectContext) -> Item {
        let newItem = Item(context: context)
        newItem.id = "123"
        newItem.timestamp = Date()
        
        let comments = (0..<3).map { commentInt -> Comment in
            let comment = Comment(context: context)
            comment.id = "\(commentInt)"
            comment.timestamp = Date()
            
            let votes = (0..<3).map { voteInt -> Vote in
                let vote = Vote(context: context)
                vote.id = "\(commentInt)-\(voteInt)"
                vote.timestamp = Date()
                return vote
            }
            
//            comment.mutableOrderedSetValue(forKey: "votes").addObjects(from: votes)
            comment.addToVotes(.init(array: votes))
            
            return comment
        }
        
        newItem.addToComments(.init(array: comments))
        
        assert((newItem.comments!.array as! [Comment]).map(\.id!) == ["0", "1", "2"])
        
        return newItem
    }
}

extension NSManagedObjectContext {
    func fetchAll<T: NSFetchRequestResult>(entity: T.Type) -> [T] {
        let fetchRequest = NSFetchRequest<T>(entityName: String(describing: T.self))
        let results = try? fetch(fetchRequest)
        return results ?? []
    }
}
